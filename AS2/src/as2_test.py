
import cv2
import sys
import os
import numpy as np
import imutils

def staticImage():
    picturePath = sys.argv[1]
    frame = cv2.imread(picturePath)
    original = frame.copy()
    
    
    colorChannelToggle = 1
    while(True):
        cv2.imshow('fame',frame)
        key = cv2.waitKey(1) & 0xFF
        if key == ord('q'):
            break
        elif key == ord('i'):
            frame = original.copy()
        elif key == ord('w'):
            cv2.imwrite("out.jpg",frame)
        elif key == ord('g'):
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        elif key == ord('c'):
            #toggle to red
            if colorChannelToggle == 1:
                frame = original.copy()
                frame[:,:,0] = 0
                frame[:,:,1] = 0
                colorChannelToggle = 2
                
            #toggle to green
            elif colorChannelToggle ==2:
                frame = original.copy()
                frame[:,:,0] = 0
                frame[:,:,2] = 0
                colorChannelToggle = 3
                
            #toggle to blue
            elif colorChannelToggle ==3:
                frame = original.copy()
                frame[:,:,1] = 0
                frame[:,:,2] = 0
                colorChannelToggle = 1
        elif key == ord('s'):
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            frame = cv2.blur(frame,(5,5))
        elif key == ord('d'):
            frame = cv2.pyrDown(frame,2)
        elif key == ord('D'):
            frame = cv2.blur(frame,(5,5))
            frame = cv2.pyrDown(frame,2)
        elif key == ord('r'):
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            frame = imutils.rotate_bound(frame,90)
        elif key == ord('h'):
            textColor = (255,0,255)
            cv2.putText(frame,"This program is able to perform live image processing",(0, 0), cv2.FONT_HERSHEY_PLAIN, 1.0,textColor, thickness =1)
            cv2.putText(frame,"You can perform different operations on the live capture  ",(100, 300), cv2.FONT_HERSHEY_PLAIN, 2.0,textColor, thickness =2)
            cv2.putText(frame," by pressing following keys on the keyboard",(200, 500), cv2.FONT_HERSHEY_PLAIN, 2.0,textColor, thickness =2)
            cv2.putText(frame,"i w g c s d D r h",(350, 400), cv2.FONT_HERSHEY_PLAIN, 2.0,(000,255,100), thickness =3)
            
            cv2.putText(frame,"USE: q FOR EXITING THE PROGRAMING  ",(400, 100), cv2.FONT_HERSHEY_PLAIN, 2.0,(0,255,255), thickness =2)
            cv2.putText(frame,"CommandLine argument \"car.jpg\"",(500, 600), cv2.FONT_HERSHEY_PLAIN, 2.0,(0,255,0), thickness =2)
                       
    cv2.destroyAllWindows()

def liveImage():
    cap = cv2.VideoCapture(0)
    ret, original = cap.read() 
    frame = original.copy()
    key=1
    colorChannelToggle = 1
    while(True):
     
        #ret, frame = cap.read()    
        ret, original = cap.read() 
        cv2.imshow('fame',frame)
        #key = cv2.waitKey(1) & 0xFF
        #if(key != 255):
         #   old_key = key
        new_key = cv2.waitKey(1) & 0xFF
        colourToggle=ord('c')
        #need to handle w here as it needs to continue the same processing
        if (new_key == ord('w')):
            cv2.imwrite("out.jpg",frame)
            new_key = 255
        elif(new_key != 255):
            key = new_key
        
        
        #initial capture case
        if key == 1:
            frame = original.copy()
            
        elif key == ord('q'):
            break
        
        elif key == ord('i'):
            frame = original.copy()
            cv2.imshow('fame',frame)
            
        elif key == ord('c'):
            
            #toggle to red
           if ((colorChannelToggle == 1) and (new_key !=255)):
               frame = original.copy()
               frame[:,:,0] = 0
               frame[:,:,1] = 0
               colorChannelToggle = 2
                
               #toggle to green
           elif ((colorChannelToggle ==2) and (new_key != 255)):
               frame = original.copy()
               frame[:,:,0] = 0
               frame[:,:,2] = 0
               colorChannelToggle = 3
                
            #toggle to blue
           elif ((colorChannelToggle ==3) and (new_key != 255)):
               frame = original.copy()
               frame[:,:,1] = 0
               frame[:,:,2] = 0
               colorChannelToggle = 1
            
        #elif key == ord('w'):
         #   cv2.imwrite("out.jpg",frame)
          #  key =255
           # frame = original.copy()
        elif key == ord('g'):
            #frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            frame = cv2.cvtColor(original, cv2.COLOR_BGR2GRAY)
        elif key == ord('s'):
            frame = cv2.cvtColor(original, cv2.COLOR_BGR2GRAY)
            frame = cv2.blur(frame,(5,5))
        elif key == ord('d'):
            frame = cv2.pyrDown(original,2)
        elif key == ord('r'):
            frame = cv2.cvtColor(original, cv2.COLOR_BGR2GRAY)
            frame = imutils.rotate_bound(frame,90)
        elif key == ord('h'):
            textColor = (255,0,255)
            cv2.putText(original,"This program is able to perform live image processing",(0,100), cv2.FONT_HERSHEY_PLAIN, 1.5,textColor, thickness =2)
            cv2.putText(original,"You can perform operations on the live capture  ",(0, 125), cv2.FONT_HERSHEY_PLAIN, 1.5,textColor, thickness =2)
            cv2.putText(original," by pressing following keys on the keyboard",(0, 150), cv2.FONT_HERSHEY_PLAIN, 1.5,textColor, thickness =2)
            cv2.putText(original,"i w g c s d D r h",(0, 175), cv2.FONT_HERSHEY_PLAIN, 1.5,(000,255,100), thickness =3)
            
            cv2.putText(original,"USE: q FOR EXITING THE PROGRAMING  ",(0, 200), cv2.FONT_HERSHEY_PLAIN, 1.5,(0,255,255), thickness =2)
            frame = original.copy()
            

    cap.release()
    cv2.destroyAllWindows()


if len(sys.argv) == 2:
    staticImage()
else:
    liveImage()

        


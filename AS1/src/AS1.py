
# coding: utf-8

# In[75]:

import numpy as np
import math

A = np.matrix([[1],[2],[3]])
B = np.matrix([[4],[5],[6]])
C = np.matrix([[-1],[1],[3]])

print("1)") 
print((2*A)-B)
print("\n") 
print("2)|A|")
magnitude = np.linalg.norm(A)
print(magnitude)
print("\n")
print("angle between the vector and x axis")
D =np.arccos(1/magnitude)
print(np.rad2deg(D))
print("\n")

print("3)unit vector in the direction of A")
print(A/magnitude)
print("\n")

print("4)The direction cosine of A")
print(A/magnitude)
print("\n")

print("5)")
print("A.B")
print(np.tensordot(A,B))
print("B.A")
print(np.tensordot(A,B))
print("\n")

print("6) the angle between A and B")
theta=np.arccos(np.tensordot(A,B)/(np.linalg.norm(A)*np.linalg.norm(B)))
print(np.rad2deg(theta))
print("\n")

print("7)A vector which is perpendicular to A")
#x = np.linalg.tensorsolve(A,B)
#print(x)
print("\n")

print("8)AxB and BxA")
print(np.cross(A,B,axis=0))
print(np.cross(B,A,axis=0))
print("\n")


print("9)A vector perpendicular to A and B is")
print(np.cross(A,B,axis=0))
print("\n")

print("10)Linear Dependency between A B and C are dependent as the determinant is a zero")
matrix = np.array(
    [
        [1,2,3],
        [4,5,6],
        [-1,1,3]
    ])

print(np.linalg.det(matrix))
v1 =  [1,2,3]
v2 = [4,5,6]
v3= [-1,1,3]
print("\n")

print("11) AtransposeB and ABtranspose")
Atranspose=A.transpose()
print(np.dot(Atranspose,B))
Btranspose=B.transpose()
print('ABtrans',np.dot(A,Btranspose))
print("\n")


# In[34]:

A = np.matrix([[1,2,3],[4,-2,3],[0,5,-1]])
B = np.matrix([[1,2,1],[2,1,-4],[3,-2,1]])
C = np.matrix([[1,2,3],[4,5,6],[-1,1,3]])

print("B")
print("1.")
print("2A-B")
print((2*A)-B)
print("\n")

print("2.")
print("AB and BA")
print(A*B)
print(B*A)
print("\n")

print("3.")
print("(AB)Transpose")
print((A*B).T)
print("\n")
print("BTransposeATranspose")
print((B.T)*(A.T))
print("\n")

print("4.")
print("|A|")
print(np.linalg.det(A))
print("|C|")
print(np.linalg.det(C))
print("\n")

print("5.")
print("Row vectors form an orthogonal set in the matrix B ")
R1=np.array(B)[0]
R2=np.array(B)[1]
R3=np.array(B)[2]
print("R1")
print(R1)
print("R2")
print(R2)
print("R3")
print(R3)
print("R1.R2")
print(np.dot(R1,R2))
print("R1.R3")
print(np.dot(R1,R3))
print("R2.R3")
print(np.dot(R2,R3))
print("\n")

print("6.")
print("A inverse")
print(np.linalg.inv(A))
print("B inverse")
print(np.linalg.inv(B))



# In[74]:

A = np.matrix([[1,2],[3,2]])
B = np.matrix([[2,-2],[-2,5]])
print("C")
print("1.Eigen values and Eigen vectors of A")
valAndvect=np.linalg.eig(A)
val=valAndvect[0]
vect=valAndvect[1]
print("Eigen values",val)
print("Eigen vect\n",vect)
print("\n")

print("2.")
print("VInverse A V")
z=np.linalg.inv(np.matrix(vect))

print(z*A*np.matrix(vect))
print("\n")

print("3. dot product between the eigenvectors of A")
v1=np.array([vect.item(0),vect.item(2)])
v2=np.array([vect.item(1),vect.item(3)])
print(np.dot(v1,v2))
print("\n")

print("4. dot product between the eigenvectors of B")
valAndvectB=np.linalg.eig(B)
vectB=valAndvectB[1]
v1B=np.array([vectB.item(0),vectB.item(2)])
v2B=np.array([vectB.item(1),vectB.item(3)])
print(np.dot(v1B,v2B))
print("\n")

print("5. Since B is a symmetrix real matrix, its eigen vectors are orthogonal. which is seen by its dot product")
print(v1B)
print(v2B)
print(np.dot(v1B,v2B))


# In[ ]:



